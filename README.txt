
This module adds support for the Tudou video sharing service, available at 
http://www.tudou.com/

To use this module, you'll first need to install Embedded Video Field, which is
packaged with Embedded Media Field (from http://drupal.org/project/media).

Set up a content type to use a Third Party Video field as you normally would 
with Media field. Also ensure that you have enabled the new Tudou provider from the 
admin screen at /admin/content/media.

@TODO: This file would need to be further completed.